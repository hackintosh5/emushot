import asyncio
import adb_shell.transport.base_transport_async
import concurrent
import functools

class UsbTransportAsync(adb_shell.transport.base_transport_async.BaseTransportAsync):
    def __init__(self, usb_transport):
        self._transport = usb_transport
        self._pool = concurrent.futures.ThreadPoolExecutor(1, "usb_worker")
        self._loop = asyncio.get_running_loop()

    def _call(self, func, *args, **kwargs):
        if kwargs:
            func = functools.partial(func, *args, **kwargs)
            args = ()
        return self._loop.run_in_executor(self._pool, func, *args, **kwargs)

    def bulk_read(self, *args, **kwargs):
        return self._call(self._transport.bulk_read, *args, **kwargs)

    def bulk_write(self, *args, **kwargs):
        return self._call(self._transport.bulk_write, *args, **kwargs)

    def close(self, *args, **kwargs):
        return self._call(self._transport.close, *args, **kwargs)

    def connect(self, *args, **kwargs):
        return self._call(self._transport.connect, *args, **kwargs)
