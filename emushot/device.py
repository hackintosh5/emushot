import asyncio

import telnetlib3

from adb_shell.adb_device_async import AdbDeviceAsync
from adb_shell.transport.tcp_transport_async import TcpTransportAsync
from adb_shell.transport.usb_transport import UsbTransport
from .usb_transport_async import UsbTransportAsync


class Device:
    def __init__(self, *args, **kwargs):
        self._args = args
        self._kwargs = kwargs

    async def _init(self):
        await self.adb.connect()
        self._stayon_state = (await self.adb.shell("settings get global stay_on_while_plugged_in")).strip()
        await self.adb.shell("svc power stayon usb")
        self.manufacturer = await self._get_property("ro.product.manufacturer")
        self.model = await self._get_property("ro.product.model")

    async def _get_property(self, prop):
        return (await self.adb.shell("getprop " + prop))[:-1]

    async def screenshot(self, output_path):
        with open(output_path, "wb") as output:
            async for chunk in self.adb.streaming_shell("screencap -p", decode=False):
                output.write(chunk)

    async def close(self):
        await self.adb.shell("settings put global stay_on_while_plugged_in " + self._stayon_state)

    async def __aenter__(self):
        await self._init(*self._args, **self._kwargs)
        return self

    async def __aexit__(self, type, value, traceback):
        await self.close()

class EmulatorDevice(Device):
    async def _check_ok(self):
        assert await self.telnet[0].readline() == b"OK"

    async def telnet_cmd(self, cmd):
        self.telnet[1].write(cmd + b"\r\n")
        self._check_ok()

    async def _init(self, adb_port, telnet_port):
        self.adb = AdbDeviceAsync(TcpTransportAsync("localhost", adb_port))
        telnet_connection = await telnet3.open_connection("localhost", telnet_port)
        await super()._init()
        line = b""
        while not line.startswith(b"'"):
            line = await self.telnet[0].readline()
        token_path = ast.literal_eval(line)
        assert isinstance(token_path, str), type(token_path)
        with open(token_path, "rb") as token_file:
            token = token_file.read()
        assert len(token) == 16, token
        await ret.telnet_cmd(b"AUTH " + token)

    async def screenshot(self, output_path):
        self.telnet_cmd(b"screenrecord screenshot " + output_path.encode("utf-8"))


class UsbDevice(Device):
    async def _init(self, adb_serial):
        self.adb = AdbDeviceAsync(UsbTransportAsync(UsbTransport.find_adb(adb_serial)))
        await super()._init()
