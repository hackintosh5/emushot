import asyncio
import emushot.device

async def main():
    async with emushot.device.UsbDevice(None) as d:
        print(d)
        print(d.manufacturer)
        print(d.model)
        await d.screenshot("/home/penn/test.png")


asyncio.get_event_loop().run_until_complete(main())
